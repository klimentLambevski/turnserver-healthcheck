require('dotenv').config();
const {app} = require('./app/app');
const https = require('https');
const fs = require('fs');
const path = require('path');

const domain = process.env.DOMAIN;
const privateKey = fs.readFileSync(path.join(__dirname, `/../certbot/config/live/${domain}/privkey.pem`), 'utf8');
const certificate = fs.readFileSync(path.join(__dirname, `/../certbot/config/live/${domain}/cert.pem`), 'utf8');
const ca = fs.readFileSync(path.join(__dirname, `/../certbot/config/live/${domain}/chain.pem`), 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};

const server = https.Server(credentials, app);

const port = process.env.PORT || 3443;

server.listen(port, (err) => {
    if (err) {
        return console.log(err)
    }
    return console.log(`https server is listening on ${port}`)
});

