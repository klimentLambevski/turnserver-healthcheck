const express = require('express');
const ApiRouter = express.Router();
const env = require("../lib/utils").env;
const serverName = env('serverName', 'TestServer');
const serverIp = env('serverIp', '127.0.0.1');
const serverRegion = env('serverRegion', 'default-region');

ApiRouter.get('/health-check', (req, res, next) => {
    res.status(200);
    return res.send();
});


ApiRouter.get('/me', (req, res, next) => {
    res.status(200);
    return res.send({
        serverName,
        serverIp,
        serverRegion
    })
});

module.exports = {
    ApiRouter
};
