/**
 * Executes the callback with the arguments of the previous Promise and then
 * returns the result from the previous Promise no matter if the callback promise is success or fail
 * ex.
 * User.create(*)
 *  .then(doWaitSilent((user) => sendNotification())
 *  .then((user) => {
 *    //still have the result from User.create in the next .then callback
 *  })
 * @param callback
 * @returns {function(*=): Bluebird<R>}
 */
const doWaitSilent = (callback) => (arg) =>
  callback(arg)
    .then(() => arg)
    .catch(() => arg);

const doWait = (callback) => (arg) =>
  callback(arg)
    .then((res) => arg);

const env = (variable, defaultVal) =>
  process.env[variable]? process.env[variable]: defaultVal;

module.exports = {
  doWaitSilent,
  doWait,
  env
}
