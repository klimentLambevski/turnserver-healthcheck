const express = require('express');
const cors = require('cors');
const BodyParser = require('body-parser');
const {ApiRouter} = require("./routes/api");

// APP init
const app = express();
app.use(express.static('public'))
app.use(cors());

//Parse Content-Type application/json
app.use(BodyParser.json());
app.use('/api', ApiRouter);

module.exports = {
    app
};
