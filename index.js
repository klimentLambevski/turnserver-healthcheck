require('dotenv').config();
const {app} = require('./app/app');
const http = require('http');

const server = http.Server(app);

const port = process.env.PORT || 3000;

server.listen(port, (err) => {
    if (err) {
        return console.log(err)
    }
    return console.log(`server is listening on ${port}`)
});
